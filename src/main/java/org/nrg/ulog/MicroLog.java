/*
 * org.nrg.ulog.MicroLog
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/12/14 12:45 PM
 */
package org.nrg.ulog;

import java.io.Closeable;
import java.io.IOException;


public interface MicroLog extends Closeable {
    void log(String s) throws IOException;
    void log(String s, Throwable t) throws IOException;
    boolean hasMessages();
}
