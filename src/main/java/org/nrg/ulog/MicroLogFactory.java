/*
 * org.nrg.ulog.MicroLogFactory
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/12/14 12:45 PM
 */
package org.nrg.ulog;

import java.io.IOException;


public interface MicroLogFactory {
  public MicroLog getLog(String label) throws IOException;
}
