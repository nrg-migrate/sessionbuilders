/*
 * org.nrg.io.RelativePathItem
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/12/14 12:45 PM
 */
package org.nrg.io;

public interface RelativePathItem {
  public String getRelativePath();
}
