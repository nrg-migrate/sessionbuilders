/*
 * org.nrg.io.ScanCatalogFileWriterFactory
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/12/14 12:45 PM
 */
package org.nrg.io;

import java.io.File;

public final class ScanCatalogFileWriterFactory
extends AbstractRelativePathWriterFactory implements RelativePathWriterFactory {
  public ScanCatalogFileWriterFactory(final File root) {
    super(root, "scan_%s_catalog.xml");
  }
}
